///> Created by Jonathan Getachew on Oct 21, 2017

#ifndef SORTING_H
#define SORTING_H

void bubbleSort(int*, int);
void insertionSort(int*, int);
void selectionSort(int*, int);

#endif // !SORTING_H


///> Created by Jonathan Getachew on Oct 21, 2017

#include <iostream>

void bubbleSort(int* a, int size) {
	for (int i = 0; i < size-1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (a[j] > a[j + 1]) {
				int temp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = temp;
			}
		}
	}
}

void insertionSort(int* a, int size) {

		
		for (int i = 0; i < size; i++) {			

			for (int j = i; j > 0; j--) {
				
				if (a[j] < a[j - 1]) {
					int temp = a[j];
					a[j] = a[j - 1];
					a[j - 1] = temp;
				}
				
			}
		}
	
}

void selectionSort(int* a, int size) {
	///> walker var to keep track ofposition of min
	int walker;

	for (int i = 0; i < size - 1; i++)
	{
		walker = i;//set pos_min to the current index of array

		for (int j = i + 1; j < size; j++) {

			if (a[j] < a[walker])
				walker = j; // keep track of the index that min is in for the swap
		}

		//if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
		if (walker != i)
		{
			int temp = a[i];
			a[i] = a[walker];
			a[walker] = temp;
		}
	}
}
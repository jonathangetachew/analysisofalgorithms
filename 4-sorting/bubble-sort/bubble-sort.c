///> Created by Jonathan Getachew on Oct 17, 2017

#include <stdio.h>

void bubbleSort(int[]);
void printArray(int[]);

int main() {

	int a[] = { 10, 7, 9, 3, 5, 2, 4 };

	bubbleSort(a);

	return 0;
}

void bubbleSort(int a[]) {
	int size = sizeof(a) / sizeof(int);

	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (a[j] > a[j + 1]) {
				temp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = temp;
			}
		}
	}

	printArray(a);
}

void printArray(int a[]) {
	int size = sizeof(a) / sizeof(int);

	for (int i = 0; i < size; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
}
# Python program for implementation of find-max1-max2
import sys
#sort the array first 
def sort(A): 
    for i in range(len(A)):
     
        # Find the minimum element in remaining 
        # unsorted array
        min_idx = i
        for j in range(i+1, len(A)):
            if A[min_idx] > A[j]:
                min_idx = j
                 
        # Swap the found minimum element with 
        # the first element        
        A[i], A[min_idx] = A[min_idx], A[i]


    return A

def main(): 
    A = [64, 25, 12, 22, 11]
    A = sort(A)
    N = 3

    for i in range(1, N + 1): 
        print ("MAX " + str(i) + " is: " + str(A[len(A) - i]))

    

main()
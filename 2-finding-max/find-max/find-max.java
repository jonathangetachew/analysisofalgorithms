///> Created by Jonathan Getachew on Oct 17, 2017

///> Java program for implementation of find-max
public class Max { 

	public static void main(String[] args) {
		
		int[] a = {10, 6, 9, 7, 4, 3, 5};
		
		System.out.println("The max is " + getMax(a));
		
	}
	
	public static int getMax(int[] a) {
		
		int max = a[0];
		
		for ( int i = 1; i < a.length; i++ ) {
			
			if ( a[i] > max ) max = a[i];
			
		}
		
		return max;
		
	}

}

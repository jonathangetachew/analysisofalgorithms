///> Created by Jonathan Getachew on Oct 17, 2017

///> C program for implementation of find - max

#include <stdio.h>

int getMax(int[]);

int main() {

	int a[] = { 10, 6, 9, 7, 4, 3, 5 };

	printf("The max is %d", getMax(a));

	return 0;
}

int getMax(int a[]) {
	int max = a[0];
	int size = sizeof(a) / sizeof(int);
	for (int i = 0; i < size; i++) {
		if (a[i] > max) max = a[i];
	}
	return max;
}
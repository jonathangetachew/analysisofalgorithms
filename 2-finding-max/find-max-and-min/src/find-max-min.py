# Python program for implementation of find-max-min
import sys
A = [64, 25, 12, 22, 11]

if len(A) != 0: 
    max_idx = 0
    min_idx = 0

    # Traverse through all array elements
    for i  in range(1, len(A)):
        
        # Find the maximum and the minimum elements in remaining 
        # unsorted array
        if A[max_idx] < A[i]: 
            max_idx = i

    	if A[min_idx] > A[i]: 
    		min_idx = i

 
#PRINT RESULT TO CONSOLE
print ("Max element: %d" %A[max_idx])
print ("Min element: %d" %A[min_idx])

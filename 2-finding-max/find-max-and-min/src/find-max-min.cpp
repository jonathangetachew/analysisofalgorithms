///> Created by Jonathan Getachew on Oct 17, 2017

#include <iostream>

///> Struct to hold both max 1 and max 2 numbers
struct MinMax {
	int max;
	int min;
};

MinMax findMax1Max2(int[], int);

int main() {

	int a[] = { 10, 2, 3, 7, 5,4 };

	MinMax output = findMax1Max2(a, 6);

	///> Min Max output
	std::cout << "The Min is " << output.min << " and the Max is " << output.max << std::endl;

	std::getchar();
	return 0;
}

MinMax findMinMax(int a[], int size) {
	MinMax rv;

	rv.max = rv.min = a[0];

	for (int i = 1; i < size; i++) {
		if (rv.max < a[i]) {
			rv.max = a[i];
		}
		if (rv.min > a[i]) {
			rv.min = a[i];
		}
	}

	return rv;

}
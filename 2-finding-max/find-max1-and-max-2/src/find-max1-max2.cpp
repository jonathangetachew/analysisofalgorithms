///> Created by Jonathan Getachew on Oct 17, 2017

#include <iostream>

///> Struct to hold both max 1 and max 2 numbers
struct Max1Max2 { 
	int max1;
	int max2;
};

Max1Max2 findMax1Max2(int[], int);

int main() {

	int a[] = { 10, 2, 3, 7, 5,4 };

	Max1Max2 output2 = findMax1Max2(a, 6);

	///> Max1 Max2 output
	std::cout << "The Max1 is " << output2.max1 << " and the Max2 is " << output2.max2 << std::endl;

	std::getchar();
	return 0;
}

Max1Max2 findMax1Max2(int a[], int size) {
	Max1Max2 rv;

	if (a[0] > a[1])
	{
		rv.max1 = a[0];
		rv.max2 = a[1];
	}
	else
	{
		rv.max2 = a[0];
		rv.max1 = a[1];
	}

	for (int i = 2; i < size; i++) {
		if (rv.max1 < a[i]) {
			//rv.max2 = rv.max1;
			std::cout << rv.max1 << std::endl;
			rv.max1 = a[i];
			std::cout << rv.max2 << std::endl;
		}
		if (rv.max1 > a[i] && rv.max2 < a[i]) {
			rv.max2 = a[i];
		}
	}

	return rv;

}
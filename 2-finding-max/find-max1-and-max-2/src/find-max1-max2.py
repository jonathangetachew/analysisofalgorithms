# Python program for implementation of find-max1-max2
import sys
#sort the array first 
def sort(A): 
    for i in range(len(A)):
     
        # Find the minimum element in remaining 
        # unsorted array
        min_idx = i
        for j in range(i+1, len(A)):
            if A[min_idx] > A[j]:
                min_idx = j
                 
        # Swap the found minimum element with 
        # the first element        
        A[i], A[min_idx] = A[min_idx], A[i]


    return A

def main(): 

    A = [64, 25, 12, 22, 11]
    A = sort(A)

    if len(A) >= 2: 
            print ("Max-1: %d" %A[len(A) - 1])
            print ("Max-2: %d" %A[len(A) - 2])
    elif len(A) >= 1: 
        print ("Max: %d" %A[len(A) - 1])

main()
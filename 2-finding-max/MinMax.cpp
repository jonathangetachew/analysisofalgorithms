///> Created by Jonathan Getachew on Oct 21, 2017

#include "MinMax.h"
#include "Sorting.h"

int findMax(int a[], int size) {
	int max = a[0];
	for (int i = 0; i < size; i++) {
		if (a[i] > max) max = a[i];
	}
	return max;
}

MinMax findMinMax(int a[], int size) {
	MinMax rv;

	rv.max = rv.min = a[0];

	for (int i = 1; i < size; i++) {
		if (rv.max < a[i]) {
			rv.max = a[i];
		}
		if (rv.min > a[i]) {
			rv.min = a[i];
		}
	}

	return rv;
	
}

Max1Max2 findMax1Max2(int a[], int size) {
	Max1Max2 rv;

	if (a[0] > a[1])
	{
		rv.max1 = a[0];
		rv.max2 = a[1];
	}
	else
	{
		rv.max2 = a[0];
		rv.max1 = a[1];
	}

	for (int i = 2; i < size; i++) {
		if (rv.max1 < a[i]) {
			//rv.max2 = rv.max1;
			//std::cout << rv.max1 << std::endl;
			rv.max1 = a[i];
			//std::cout << rv.max2 << std::endl;
		}
		if (rv.max1 > a[i] && rv.max2 < a[i]) {
			rv.max2 = a[i];
		}
	}

	return rv;

}

int* findNMaxes(int a[], int size/*SIZE*/, int n) {
	bubbleSort(a, size);

	int* b = new int[n];

	for (int i = 0; i < n; i++) { // Initialize all elements of the new array to 0
		b[i] = 0;
	}

	int i = size - 1;
	int j = 0;

	for ( ; j < n, i >= 0; j++, i--) {
		b[j] = a[i];
	}

	return b;

}

int findNthMax(int a[], int size, int n) {
	bubbleSort(a, size);

	int nthMax, counter = 1;

	for (int i = size - 1; i >= 0; i--, counter++) {
		if (counter == n) {
			nthMax = a[i];
		}
	}

	return nthMax;
}

int findGCD(int a, int b) {

	if (b <= a) {
		int t = a;
		a = b;
		b = t;
	}
	else {
		while (b != 0) {
			int g = a  % b; // Calculate remainder
			a = b;
			b = g;
		}
	}

	return a;

}

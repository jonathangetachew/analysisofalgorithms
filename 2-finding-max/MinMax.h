///> Created by Jonathan Getachew on Oct 21, 2017

#ifndef MINMAX_H
#define MINMAX_H

struct MinMax {
	int min;
	int max;
};

struct Max1Max2 {
	int max1;
	int max2;
};

int findMax(int[], int);
MinMax findMinMax(int[], int);
Max1Max2 findMax1Max2(int[], int);
int* findNMaxes(int[], int, int);
int findNthMax(int[], int, int);

int findGCD(int, int);

#endif // !MINMAX_H



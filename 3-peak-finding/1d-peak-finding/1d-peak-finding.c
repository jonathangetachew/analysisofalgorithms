def peakfinder(arr, i, j):


    mid = (i + j) // 2

    if arr[mid - 1] <= arr[mid] >= arr[mid + 1]:
        print 'Final value: %s is bigger than %s on the left and %s on the right.' % (arr[mid], arr[mid-1], arr[mid+1])
        return mid

    elif arr[mid - 1] > arr[mid]:
       # Go left.
       return peakfinder(arr, i, mid - 1)

    elif arr[mid] < arr[mid + 1]:
       # Go right.
       return peakfinder(arr, mid + 1, j)




def main(): 

    arr = [12, 31, 1, 4, 5, 12, 11, 2, 3, 4]

    peakfinder(arr, 0, len(arr))




main()
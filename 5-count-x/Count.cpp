///> Created by Jonathan Getachew on Oct 19, 2017

int count(int a[], int size, int key) {
	int counter = 0;	
	for(int i = 0; i < size; i++) {		
		if(a[i] == key) counter++;		
	}	
	
	return counter;
}
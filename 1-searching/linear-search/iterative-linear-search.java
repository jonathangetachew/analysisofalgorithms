///> Created by Jonathan Getachew on Oct 17, 2017

public class Search {
	public static void main(String[] args) {
		int[] a = { 1, 2, 3 , 4, 5 };
		
		System.out.println("4 is found at index " + linearSearch(a, 4));
	}
	
	public static int linearSearch(int[] a, int key) {
		for(int i = 0; i < a.length; i++) {
			if ( a[i] == key ) return i;
		}
		
		return -1;
	}	
	
}
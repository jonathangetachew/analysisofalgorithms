///> Created by Jonathan Getachew on Oct 17, 2017

#include <stdio.h>

int linearSearch(int[], int); // Returns index of element if found and -1 if not found

int main() {
	int a[] = { 1, 2, 3, 4, 5, 6, 7 };

	printf("4 is found at index %d", linearSearch(a, 4));
}

int linearSearch(int a[], int key) {

	int size = sizeof(a) / sizeof(int);

	for (int i = 0; i < size; i++) {
		if (a[i] == key) {
			return i;
		}
	}

	return -1;
}
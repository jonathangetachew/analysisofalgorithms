"""
	Search for a key in an array in a lineaner manner. 
	Parameters
		obj : integer array 
		item : integer
		start : integer 
			* optional 
			index to start search from
	returns the index of the item or returns -1 if item is not found in list
"""
def linear_search(obj, item, start=0): 
	for i in range(start, len(obj)): 
		if obj[i] == item: 
			return i
	return -1



def main (): 
	#DEFINE THE LIST
	MY_LIST = [5, 4, 3, 2, 1]

	#DEFINE THE KEY
	key = 0

	#STORE THE RESULT FROM THE SEARCH FUNCITON IN A VARIABLE
	result = linear_search(MY_LIST, key)

	#PRINT THE APPROPRIATE MESSAGE TO CONSOLE
	if result == -1: 
		print "Item: " + str(key) + " NOT FOUND"
	else: 
		print "Item: " + str(key) + " found at index: " + str(result)


#START PROGRAM
main()


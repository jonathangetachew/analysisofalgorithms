"""	
	Search through a list using binary search algorithm 

	Parameters: 
		alist : list
		item : key to look for 
	returns the index of the item found otherwise it returns -1 
"""
def binary_search(alist, item):
    first = 0
    last = len(alist)-1
    found = False
    found_index = -1
	
    while first<=last and not found:
        midpoint = (first + last)//2
        if alist[midpoint] == item:
            found = True
            found_index = midpoint
        else:
            if item < alist[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1
	
    return found_index
	
def main():
	#create the list 
	MY_LIST = [0, 1, 2, 8, 13, 17, 19, 32, 42]
	
	#define key to look for 
	key = 8

	result = binary_search(MY_LIST, key)


	
	if result == -1: 
		print "Item: " + str(key) + " NOT FOUND"
	else: 
		print "Item: " + str(key) + " found at index: " + str(result)



#start
main()